# go-csp-evaluator

A language port of https://github.com/google/csp-evaluator to evaluate Content Security Policies for a wide range of bypasses and weaknesses in Go.

## Usage

To run the validator command line tool, check out this repository and run:

- `cd cmd/validator`
- `go build`

```
Usage of ./validator:
  -input string
        input to validate, can be url, file, or raw CSP string
  -output string
        output results to JSON file
  -s    suppress output to stdout
```

Examples:


Read the `Content-Security-Policy` header from https://www.mozilla.org/en-US/firefox/new/ and report to console.
```
./validator -input "https://www.mozilla.org/en-US/firefox/new/"
```


Read the firefox_sample.txt file, output json results to `firefox.json` and suppress output to console:

```
./validator -input firefox_sample.txt -output firefox.json -s
```

Read the `Content-Security-Policy` from the command line:
```
./validator -input "script-src 'self' *.mozilla.net *.mozilla.org *.mozilla.com 'unsafe-inline' 'unsafe-eval' www.googletagmanager.com www.google-analytics.com tagmanager.google.com www.youtube.com s.ytimg.com cdn-3.convertexperiments.com app.convert.com data.track.convertexperiments.com 1003350.track.convertexperiments.com 1003343.track.convertexperiments.com cdn.cookielaw.org assets.getpocket.com; child-src 'self' *.mozilla.net *.mozilla.org *.mozilla.com www.googletagmanager.com www.google-analytics.com www.youtube-nocookie.com trackertest.org www.surveygizmo.com accounts.firefox.com accounts.firefox.com.cn www.youtube.com; frame-src 'self' *.mozilla.net *.mozilla.org *.mozilla.com www.googletagmanager.com www.google-analytics.com www.youtube-nocookie.com trackertest.org www.surveygizmo.com accounts.firefox.com accounts.firefox.com.cn www.youtube.com; connect-src 'self' *.mozilla.net *.mozilla.org *.mozilla.com www.googletagmanager.com www.google-analytics.com logs.convertexperiments.com 1003350.metrics.convertexperiments.com 1003343.metrics.convertexperiments.com sentry.prod.mozaws.net o1069899.sentry.io cdn.cookielaw.org privacyportal.onetrust.com https://accounts.firefox.com/ getpocket.com stage.cjms.nonprod.cloudops.mozgcp.net cjms.services.mozilla.com; font-src 'self' assets.getpocket.com; style-src 'self' *.mozilla.net *.mozilla.org *.mozilla.com 'unsafe-inline' app.convert.com; default-src 'self' *.mozilla.net *.mozilla.org *.mozilla.com; img-src 'self' *.mozilla.net *.mozilla.org *.mozilla.com data: mozilla.org www.googletagmanager.com www.google-analytics.com adservice.google.com adservice.google.de adservice.google.dk creativecommons.org cdn-3.convertexperiments.com logs.convertexperiments.com images.ctfassets.net cdn.cookielaw.org ad.doubleclick.net"
```