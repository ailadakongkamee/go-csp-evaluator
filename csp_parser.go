package gocspevaluator

import (
	"regexp"
	"strings"
)

var nameValuePattern = regexp.MustCompile(`\S+`)

type CSPParser struct {
	csp      CSP
	cspValue string
}

func NewCSPParser(unparsed string) CSPParser {
	return CSPParser{csp: NewCSP(), cspValue: unparsed}
}

// Parse a CSP from a string.
func (c *CSPParser) Parse() CSP {

	directiveTokens := strings.Split(c.cspValue, ";")
	for i := 0; i < len(directiveTokens); i++ {
		directiveToken := strings.TrimSpace(directiveTokens[i])

		// Split directive tokens into directive name and directive values.
		directiveParts := nameValuePattern.FindAllString(directiveToken, -1)
		if len(directiveParts) > 0 {
			directiveName := strings.ToLower(directiveParts[0])

			// If the set of directives already contains a directive whose name is a
			// case insensitive match for directive name, ignore this instance of
			// the directive and continue to the next token.
			if exist := c.csp.GetDirective(Directive(directiveName)); exist != nil {
				continue
			}

			// ?? dead code i guess?
			// if (!IsDirective(directiveName)) {}

			directiveValues := make([]string, 0)
			for j := 1; ; j++ {
				if j > len(directiveParts)-1 {
					break
				}
				directiveValue := NormalizeDirectiveValue(directiveParts[j])
				if !Includes(directiveValues, directiveValue) {
					directiveValues = append(directiveValues, directiveValue)
				}
			}
			c.csp.directives.Set(Directive(directiveName), directiveValues)
		}
	}
	return c.csp
}

func NormalizeDirectiveValue(directiveValue string) string {
	directiveValue = strings.TrimSpace(directiveValue)
	directiveValueLower := strings.ToLower(directiveValue)
	if IsKeyword(directiveValueLower) || IsUrlScheme(directiveValue) {
		return directiveValueLower
	}
	return directiveValue
}
