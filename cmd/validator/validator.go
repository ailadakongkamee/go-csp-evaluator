package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	gocspevaluator "gitlab.com/gitlab-org/vulnerability-research/foss/go-csp-evaluator"
)

var (
	input    string
	output   string
	suppress bool
)

func init() {
	flag.StringVar(&input, "input", "", "input to validate, can be url, file, or raw CSP string")
	flag.StringVar(&output, "output", "", "output results to JSON file")
	flag.BoolVar(&suppress, "s", false, "suppress output to stdout")
}

func main() {
	flag.Parse()

	var findings []gocspevaluator.Finding

	if strings.HasPrefix(input, "http") {
		findings = validateURL()
	} else if _, err := os.Stat(input); err == nil {
		findings = validateFile()
	} else {
		findings = validateCSP(input)
	}

	if !suppress {
		for _, finding := range findings {
			log.Printf("%#v\n", finding)
		}
	}

	if output == "" {
		return
	}

	dat, err := json.Marshal(findings)
	if err != nil {
		log.Fatalf("failed to marshal finding output: %s\n", err)
	}

	if err := os.WriteFile(output, dat, 0644); err != nil {
		log.Fatalf("failed to write output file: %s\n", err)
	}
}

func validateURL() []gocspevaluator.Finding {
	resp, err := http.Get(input)
	if err != nil {
		log.Fatalf("failed to get URL: %s\n", err)
	}
	defer resp.Body.Close()

	var findings []gocspevaluator.Finding

	cspHeader := resp.Header.Get("content-security-policy")
	if cspHeader != "" {
		log.Printf("Validating Content-Security-Policy header")
		findings = validateCSP(cspHeader)
	}

	return findings
}

func validateFile() []gocspevaluator.Finding {
	dat, err := ioutil.ReadFile(input)
	if err != nil {
		log.Fatalf("Failed to read file: %s\n", err)
	}

	return validateCSP(string(dat))
}

func validateCSP(input string) []gocspevaluator.Finding {

	parser := gocspevaluator.NewCSPParser(input)
	parsedCSP := parser.Parse()
	eval := gocspevaluator.NewCSPEvaluator(parsedCSP, gocspevaluator.CSP3)
	findings := eval.Evaluate([]gocspevaluator.CheckerFn{gocspevaluator.CheckRequiresTrustedTypesForScripts}, gocspevaluator.DEFAULT_CHECKS)

	return findings
}
