package gocspevaluator

import "regexp"

var URL_SCHEME_PATTERN = regexp.MustCompile(`^[a-zA-Z][+a-zA-Z0-9.-]*:$`)

/**
 * Checks if a string is a valid URL scheme.
 * Scheme part + ":"
 * For scheme part see https://tools.ietf.org/html/rfc3986#section-3.1
 * @param urlScheme value to check.
 * @return True if urlScheme has a valid scheme.
 */
func IsUrlScheme(urlScheme string) bool {
	return URL_SCHEME_PATTERN.MatchString(urlScheme)
}

/**
 * A regex pattern to check nonce prefix and Base64 formatting of a nonce value.
 */
var STRICT_NONCE_PATTERN = regexp.MustCompile(`^\'nonce-[a-zA-Z0-9+/_-]+[=]{0,2}\'$`)

/** A regex pattern for checking if nonce prefix. */
var NONCE_PATTERN = regexp.MustCompile(`^\'nonce-(.+)\'$`)

func IsNonce(nonce string, strictCheck bool) bool {
	if strictCheck {
		return STRICT_NONCE_PATTERN.MatchString(nonce)
	}
	return NONCE_PATTERN.MatchString(nonce)
}

/**
 * A regex pattern to check hash prefix and Base64 formatting of a hash value.
 */
var STRICT_HASH_PATTERN = regexp.MustCompile(`^\'(sha256|sha384|sha512)-[a-zA-Z0-9+/]+[=]{0,2}\'$`)

/** A regex pattern to check hash prefix. */
var HASH_PATTERN = regexp.MustCompile(`^\'(sha256|sha384|sha512)-(.+)\'$`)

func IsHash(hash string, strictCheck bool) bool {
	if strictCheck {
		return STRICT_HASH_PATTERN.MatchString(hash)
	}
	return HASH_PATTERN.MatchString(hash)
}

type TrustedTypesSink string

const (
	SCRIPT TrustedTypesSink = "'script'"
)

type Directive string

func (d Directive) String() string {
	return string(d)
}

const (
	// Fetch directives
	CHILD_SRC       Directive = "child-src"
	CONNECT_SRC     Directive = "connect-src"
	DEFAULT_SRC     Directive = "default-src"
	FONT_SRC        Directive = "font-src"
	FRAME_SRC       Directive = "frame-src"
	IMG_SRC         Directive = "img-src"
	MEDIA_SRC       Directive = "media-src"
	OBJECT_SRC      Directive = "object-src"
	SCRIPT_SRC      Directive = "script-src"
	SCRIPT_SRC_ATTR Directive = "script-src-attr"
	SCRIPT_SRC_ELEM Directive = "script-src-elem"
	STYLE_SRC       Directive = "style-src"
	STYLE_SRC_ATTR  Directive = "style-src-attr"
	STYLE_SRC_ELEM  Directive = "style-src-elem"
	PREFETCH_SRC    Directive = "prefetch-src"

	MANIFEST_SRC Directive = "manifest-src"
	WORKER_SRC   Directive = "worker-src"

	// Document directives
	BASE_URI      Directive = "base-uri"
	PLUGIN_TYPES  Directive = "plugin-types"
	SANDBOX       Directive = "sandbox"
	DISOWN_OPENER Directive = "disown-opener"

	// Navigation directives
	FORM_ACTION     Directive = "form-action"
	FRAME_ANCESTORS Directive = "frame-ancestors"
	NAVIGATE_TO     Directive = "navigate-to"

	// Reporting directives
	REPORT_TO  Directive = "report-to"
	REPORT_URI Directive = "report-uri"

	// Other directives
	BLOCK_ALL_MIXED_CONTENT   Directive = "block-all-mixed-content"
	UPGRADE_INSECURE_REQUESTS Directive = "upgrade-insecure-requests"
	REFLECTED_XSS             Directive = "reflected-xss"
	REFERRER                  Directive = "referrer"
	REQUIRE_SRI_FOR           Directive = "require-sri-for"
	TRUSTED_TYPES             Directive = "trusted-types"
	// https://github.com/WICG/trusted-types
	REQUIRE_TRUSTED_TYPES_FOR Directive = "require-trusted-types-for"
	WEBRTC                    Directive = "webrtc"
)

var allDirectives = map[Directive]bool{
	// Fetch directives
	CHILD_SRC:       true,
	CONNECT_SRC:     true,
	DEFAULT_SRC:     true,
	FONT_SRC:        true,
	FRAME_SRC:       true,
	IMG_SRC:         true,
	MEDIA_SRC:       true,
	OBJECT_SRC:      true,
	SCRIPT_SRC:      true,
	SCRIPT_SRC_ATTR: true,
	SCRIPT_SRC_ELEM: true,
	STYLE_SRC:       true,
	STYLE_SRC_ATTR:  true,
	STYLE_SRC_ELEM:  true,
	PREFETCH_SRC:    true,

	MANIFEST_SRC: true,
	WORKER_SRC:   true,

	// Document directives
	BASE_URI:      true,
	PLUGIN_TYPES:  true,
	SANDBOX:       true,
	DISOWN_OPENER: true,

	// Navigation directives
	FORM_ACTION:     true,
	FRAME_ANCESTORS: true,
	NAVIGATE_TO:     true,

	// Reporting directives
	REPORT_TO:  true,
	REPORT_URI: true,

	// Other directives
	BLOCK_ALL_MIXED_CONTENT:   true,
	UPGRADE_INSECURE_REQUESTS: true,
	REFLECTED_XSS:             true,
	REFERRER:                  true,
	REQUIRE_SRI_FOR:           true,
	TRUSTED_TYPES:             true,
	// https://github.com/WICG/trusted-types
	REQUIRE_TRUSTED_TYPES_FOR: true,
	WEBRTC:                    true,
}

func IsDirective(directive string) bool {
	_, exist := allDirectives[Directive(directive)]
	return exist
}

var FetchDirectives = []Directive{
	CHILD_SRC, CONNECT_SRC, DEFAULT_SRC,
	FONT_SRC, FRAME_SRC, IMG_SRC,
	MANIFEST_SRC, MEDIA_SRC, OBJECT_SRC,
	SCRIPT_SRC, SCRIPT_SRC_ATTR, SCRIPT_SRC_ELEM,
	STYLE_SRC, STYLE_SRC_ATTR, STYLE_SRC_ELEM,
	WORKER_SRC,
}

func FetchDirectivesIncludes(directive Directive) bool {
	for _, dir := range FetchDirectives {
		if dir == directive {
			return true
		}
	}
	return false
}
