package gocspevaluator

type Keyword string

const (
	KW_SELF                     Keyword = "'self'"
	KW_NONE                     Keyword = "'none'"
	KW_UNSAFE_INLINE            Keyword = "'unsafe-inline'"
	KW_UNSAFE_EVAL              Keyword = "'unsafe-eval'"
	KW_WASM_EVAL                Keyword = "'wasm-eval'"
	KW_WASM_UNSAFE_EVAL         Keyword = "'wasm-unsafe-eval'"
	KW_STRICT_DYNAMIC           Keyword = "'strict-dynamic'"
	KW_UNSAFE_HASHED_ATTRIBUTES Keyword = "'unsafe-hashed-attributes'"
	KW_UNSAFE_HASHES            Keyword = "'unsafe-hashes'"
	KW_REPORT_SAMPLE            Keyword = "'report-sample'"
	KW_BLOCK                    Keyword = "'block'"
	KW_ALLOW                    Keyword = "'allow'"
)

var allKeywords = map[Keyword]bool{
	KW_SELF:                     true,
	KW_NONE:                     true,
	KW_UNSAFE_INLINE:            true,
	KW_UNSAFE_EVAL:              true,
	KW_WASM_EVAL:                true,
	KW_WASM_UNSAFE_EVAL:         true,
	KW_STRICT_DYNAMIC:           true,
	KW_UNSAFE_HASHED_ATTRIBUTES: true,
	KW_UNSAFE_HASHES:            true,
	KW_REPORT_SAMPLE:            true,
	KW_BLOCK:                    true,
	KW_ALLOW:                    true,
}

func (k Keyword) String() string {
	return string(k)
}

func IsKeyword(keyword string) bool {
	_, exist := allKeywords[Keyword(keyword)]
	return exist
}
