package gocspevaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func testCheckCSP(testPolicy string, checkFn CheckerFn) []Finding {
	parser := NewCSPParser(testPolicy)
	parsedCSP := parser.Parse()
	return checkFn(parsedCSP)
}

func TestCheckUnknownDirective(t *testing.T) {
	policy := "foobar-src http:"
	findings := testCheckCSP(policy, CheckUnknownDirective)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_SYNTAX, findings[0].Severity)
	assert.Equal(t, Directive("foobar-src"), findings[0].Directive)
}

func TestCheckMissingSemicolon(t *testing.T) {
	policy := "default-src foo.bar script-src 'none'"
	findings := testCheckCSP(policy, CheckMissingSemicolon)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_SYNTAX, findings[0].Severity)
	assert.Equal(t, "script-src", findings[0].Value)
}

func TestCheckInvalidKeywordForgottenSingleTicks(t *testing.T) {
	policy := "script-src strict-dynamic nonce-test sha256-asdf"
	findings := testCheckCSP(policy, CheckInvalidKeyword)
	assert.Len(t, findings, 3)
	for _, f := range findings {
		assert.Equal(t, SEV_SYNTAX, f.Severity)
		assert.Contains(t, f.Description, "single-ticks")
	}
}

func TestCheckInvalidKeywordUnknownKeyword(t *testing.T) {
	policy := "script-src 'foo-bar'"
	findings := testCheckCSP(policy, CheckInvalidKeyword)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_SYNTAX, findings[0].Severity)
	assert.Equal(t, "'foo-bar'", findings[0].Value)
}

func TestCheckInvalidKeywordAllowsRequireTrustedTypesForScript(t *testing.T) {
	policy := "require-trusted-types-for 'script'"
	findings := testCheckCSP(policy, CheckInvalidKeyword)
	assert.Len(t, findings, 0)
}

func TestCheckInvalidKeywordAllowsTrustedTypesAllowDuplicateKeyword(t *testing.T) {
	policy := "trusted-types 'allow-duplicates' policy1"
	findings := testCheckCSP(policy, CheckInvalidKeyword)
	assert.Len(t, findings, 0)
}
