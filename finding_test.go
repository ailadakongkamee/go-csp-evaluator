package gocspevaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFinding(t *testing.T) {
	f := Finding{FindingType: MISSING_SEMICOLON,
		Description: "description",
		Severity:    SEV_HIGH,
		Directive:   SCRIPT_SRC,
		Value:       KW_NONE.String(),
	}
	assert.Equal(t, MISSING_SEMICOLON, f.FindingType)
	assert.Equal(t, "description", f.Description)
	assert.Equal(t, SEV_HIGH, f.Severity)
	assert.Equal(t, SCRIPT_SRC, f.Directive)
	assert.Equal(t, KW_NONE.String(), f.Value)
	assert.True(t, f.Equals(f))
}

func TestFindingHighestSeverity(t *testing.T) {
	f1 := Finding{FindingType: MISSING_SEMICOLON,
		Description: "description",
		Severity:    SEV_HIGH,
		Directive:   SCRIPT_SRC,
		Value:       KW_NONE.String(),
	}
	f2 := Finding{FindingType: MISSING_SEMICOLON,
		Description: "description",
		Severity:    SEV_MEDIUM,
		Directive:   SCRIPT_SRC,
		Value:       KW_NONE.String(),
	}
	f3 := Finding{FindingType: MISSING_SEMICOLON,
		Description: "description",
		Severity:    SEV_INFO,
		Directive:   SCRIPT_SRC,
		Value:       KW_NONE.String(),
	}

	f := Finding{}
	assert.Equal(t, SEV_HIGH, f.HighestSeverity([]Finding{f1, f3, f2, f1}))

	assert.Equal(t, SEV_MEDIUM, f.HighestSeverity([]Finding{f3, f2}))

	assert.Equal(t, SEV_INFO, f.HighestSeverity([]Finding{f3, f3}))

	assert.Equal(t, SEV_NONE, f.HighestSeverity([]Finding{}))
}
