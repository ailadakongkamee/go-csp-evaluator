package gocspevaluator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckScriptUnsafeInlineInScriptSrc(t *testing.T) {
	policy := "default-src https:; script-src 'unsafe-inline'"
	findings := testCheckCSP(policy, CheckScriptUnsafeInline)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
}

func TestCheckScriptUnsafeInlineInDefaultSrc(t *testing.T) {
	policy := "default-src 'unsafe-inline'"
	findings := testCheckCSP(policy, CheckScriptUnsafeInline)
	assert.Len(t, findings, 1)
}

func TestCheckScriptUnsafeInlineInDefaultSrcAndNotInScriptSrc(t *testing.T) {
	policy := "default-src 'unsafe-inline'; script-src https:"
	findings := testCheckCSP(policy, CheckScriptUnsafeInline)
	assert.Len(t, findings, 0)
}

func TestCheckScriptUnsafeInlineWithNonce(t *testing.T) {
	policy := "script-src 'unsafe-inline' 'nonce-foobar'"
	parser := NewCSPParser(policy)
	parsedCSP := parser.Parse()
	effectiveCSP := parsedCSP.EffectiveCSP(CSP1)

	findings := CheckScriptUnsafeInline(effectiveCSP)
	assert.Len(t, findings, 1)

	effectiveCSP3 := parsedCSP.EffectiveCSP(CSP3)

	findings = CheckScriptUnsafeInline(effectiveCSP3)
	assert.Len(t, findings, 0)

}

func TestCheckScriptUnsafeEvalInScriptSrc(t *testing.T) {
	policy := "default-src https:; script-src 'unsafe-eval'"
	findings := testCheckCSP(policy, CheckScriptUnsafeEval)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_MEDIUM_MAYBE, findings[0].Severity)
}

func TestCheckScriptUnsafeEvalInDefaultSrc(t *testing.T) {
	policy := "default-src 'unsafe-eval'"
	findings := testCheckCSP(policy, CheckScriptUnsafeEval)
	assert.Len(t, findings, 1)
}

func TestCheckPlainUrlSchemesInScriptSrc(t *testing.T) {
	policy := "script-src data: http: https: sthInvalid:"
	findings := testCheckCSP(policy, CheckPlainUrlSchemes)
	assert.Len(t, findings, 3)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
}

func TestCheckPlainUrlSchemesInObjectSrc(t *testing.T) {
	policy := "object-src data: http: https: sthInvalid:"
	findings := testCheckCSP(policy, CheckPlainUrlSchemes)
	assert.Len(t, findings, 3)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
}

func TestCheckPlainUrlSchemesInBaseUri(t *testing.T) {
	policy := "base-uri data: http: https: sthInvalid:"
	findings := testCheckCSP(policy, CheckPlainUrlSchemes)
	assert.Len(t, findings, 3)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
}

func TestCheckPlainUrlSchemesMixed(t *testing.T) {
	policy := "default-src https:; object-src data: sthInvalid:"
	findings := testCheckCSP(policy, CheckPlainUrlSchemes)
	assert.Len(t, findings, 2)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
	assert.Equal(t, DEFAULT_SRC, findings[0].Directive)
	assert.Equal(t, OBJECT_SRC, findings[1].Directive)
}

func TestCheckPlainUrlSchemesDangerousDirectivesOK(t *testing.T) {
	policy := "default-src https:; object-src 'none'; script-src 'none'; base-uri 'none'"
	findings := testCheckCSP(policy, CheckPlainUrlSchemes)
	assert.Empty(t, findings)
}

func TestCheckWildcardsInScriptSrc(t *testing.T) {
	policy := "script-src * http://* //*"
	findings := testCheckCSP(policy, CheckWildcards)
	assert.Len(t, findings, 3)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
}

func TestCheckWildcardsInObjectSrc(t *testing.T) {
	policy := "object-src * http://* //*"
	findings := testCheckCSP(policy, CheckWildcards)
	assert.Len(t, findings, 3)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
}

func TestCheckWildcardsInBaseUri(t *testing.T) {
	policy := "base-uri * http://* //*"
	findings := testCheckCSP(policy, CheckWildcards)
	assert.Len(t, findings, 3)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
}

func TestCheckWildcardsSchemesMixed(t *testing.T) {
	policy := "default-src *; object-src * ignore.me.com"
	findings := testCheckCSP(policy, CheckWildcards)
	assert.Len(t, findings, 2)
	for _, finding := range findings {
		assert.Equal(t, SEV_HIGH, finding.Severity)
	}
	assert.Equal(t, DEFAULT_SRC, findings[0].Directive)
	assert.Equal(t, OBJECT_SRC, findings[1].Directive)
}

func TestCheckWildcardsDangerousDirectivesOK(t *testing.T) {
	policy := "default-src *; object-src *.foo.bar; script-src 'none'; base-uri 'none'"
	findings := testCheckCSP(policy, CheckWildcards)
	assert.Empty(t, findings)
}

func TestCheckMissingDirectivesMissingObjectSrc(t *testing.T) {
	policy := "object-src 'none'"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
}

func TestCheckMissingDirectivesObjectSrcSelf(t *testing.T) {
	policy := "object-src 'self'"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
}

func TestCheckMissingDirectivesMissingBaseUriInNonceCsp(t *testing.T) {
	policy := "script-src 'nonce-123'; object-src 'none'"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
}

func TestCheckMissingDirectivesMissingBaseUriInHashWStrictDynamicCsp(t *testing.T) {
	policy := "script-src 'sha256-123456' 'strict-dynamic'; object-src 'none'"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
}

func TestCheckMissingDirectivesMissingBaseUriInHashCsp(t *testing.T) {
	policy := "script-src 'sha256-123456'; object-src 'none'"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Empty(t, findings)
}

func TestCheckMissingDirectivesScriptAndObjectSrcSet(t *testing.T) {
	policy := "script-src 'none'; object-src 'none'"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Empty(t, findings)
}

func TestCheckMissingDirectivesDefaultSrcSet(t *testing.T) {
	policy := "default-src https:;"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Empty(t, findings)
}

func TestCheckMissingDirectivesDefaultSrcSetToNone(t *testing.T) {
	policy := "default-src 'none';"
	findings := testCheckCSP(policy, CheckMissingDirectives)
	assert.Empty(t, findings)
}

func TestCheckScriptAllowlistBypassJSONPBypass(t *testing.T) {
	policy := "script-src *.google.com"
	findings := testCheckCSP(policy, CheckScriptAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
	assert.Contains(t, findings[0].Description, `www.google.com is known to host JSONP endpoints which`)
}

func TestCheckScriptAllowlistBypassWithNoneAndJSONPBypass(t *testing.T) {
	policy := "script-src *.google.com 'none'"
	findings := testCheckCSP(policy, CheckScriptAllowlistBypass)
	assert.Empty(t, findings)
}

func TestCheckScriptAllowlistBypassJSONPBypassEvalRequired(t *testing.T) {
	policy := "script-src https://googletagmanager.com 'unsafe-eval'"
	findings := testCheckCSP(policy, CheckScriptAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
}

func TestCheckScriptAllowlistBypassJSONPBypassEvalRequiredNotPresent(t *testing.T) {
	policy := "script-src https://googletagmanager.com"
	findings := testCheckCSP(policy, CheckScriptAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_MEDIUM_MAYBE, findings[0].Severity)
}

func TestCheckScriptAllowlistBypassAngularBypass(t *testing.T) {
	policy := "script-src gstatic.com"
	findings := testCheckCSP(policy, CheckScriptAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
	assert.Contains(t, findings[0].Description, `gstatic.com is known to host Angular libraries which`)
}

func TestCheckScriptAllowlistBypassNoBypassWarningOnly(t *testing.T) {
	policy := "script-src foo.bar"
	findings := testCheckCSP(policy, CheckScriptAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_MEDIUM_MAYBE, findings[0].Severity)
}

func TestCheckScriptAllowlistBypassNoBypassSelfWarningOnly(t *testing.T) {
	policy := "script-src 'self'"
	findings := testCheckCSP(policy, CheckScriptAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_MEDIUM_MAYBE, findings[0].Severity)
}

func TestCheckFlashObjectAllowlistBypassFlashBypass(t *testing.T) {
	policy := "object-src https://*.googleapis.com"
	findings := testCheckCSP(policy, CheckFlashObjectAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_HIGH, findings[0].Severity)
}

func TestCheckFlashObjectAllowlistBypassNoFlashBypass(t *testing.T) {
	policy := "object-src https://foo.bar"
	findings := testCheckCSP(policy, CheckFlashObjectAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_MEDIUM_MAYBE, findings[0].Severity)
}

func TestCheckFlashObjectAllowlistBypassSelfAllowed(t *testing.T) {
	policy := "object-src 'self'"
	findings := testCheckCSP(policy, CheckFlashObjectAllowlistBypass)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_MEDIUM_MAYBE, findings[0].Severity)
	assert.Contains(t, findings[0].Description, `Can you restrict object-src to 'none' only?`)
}

func TestCheckIpSource(t *testing.T) {
	policy := "script-src 8.8.8.8; font-src //127.0.0.1 https://[::1] not.an.ip"
	findings := testCheckCSP(policy, CheckIPSource)
	assert.Len(t, findings, 3)
	for _, finding := range findings {
		assert.Equal(t, SEV_INFO, finding.Severity)
	}
}

func TestLooksLikeIpAddressIPv4(t *testing.T) {
	assert.True(t, LooksLikeIPAddress("8.8.8.8"))
}

func TestLooksLikeIpAddressIPv6(t *testing.T) {
	assert.True(t, LooksLikeIPAddress("[::1]"))
	assert.False(t, LooksLikeIPAddress("[]"))
}

func TestCheckDeprecatedDirectiveReportUriWithReportTo(t *testing.T) {
	policy := "report-uri foo.bar/csp;report-to abc"
	findings := testCheckCSP(policy, CheckDeprecatedDirective)
	assert.Len(t, findings, 0)
}

func TestCheckDeprecatedDirectiveWithoutReportUriButWithReportTo(t *testing.T) {
	policy := "report-to abc"
	findings := testCheckCSP(policy, CheckDeprecatedDirective)
	assert.Len(t, findings, 0)
}

func TestCheckDeprecatedDirectiveReflectedXss(t *testing.T) {
	policy := "reflected-xss block"
	findings := testCheckCSP(policy, CheckDeprecatedDirective)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_INFO, findings[0].Severity)
}

func TestCheckDeprecatedDirectiveReferrer(t *testing.T) {
	policy := "referrer origin"
	findings := testCheckCSP(policy, CheckDeprecatedDirective)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_INFO, findings[0].Severity)
}

func TestCheckNonceLengthWithLongNonce(t *testing.T) {
	policy := "script-src 'nonce-veryLongRandomNonce'"
	findings := testCheckCSP(policy, CheckNonceLength)
	assert.Len(t, findings, 0)
}

func TestCheckNonceLengthWithShortNonce(t *testing.T) {
	policy := "script-src 'nonce-short'"
	findings := testCheckCSP(policy, CheckNonceLength)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_MEDIUM, findings[0].Severity)
}

func TestCheckNonceLengthInvalidCharset(t *testing.T) {
	policy := "script-src 'nonce-***notBase64***'"
	findings := testCheckCSP(policy, CheckNonceLength)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_INFO, findings[0].Severity)
}

func TestCheckSrcHttp(t *testing.T) {
	policy := "script-src http://foo.bar https://test.com; report-uri http://test.com"
	findings := testCheckCSP(policy, CheckSrcHttp)
	assert.Len(t, findings, 2)
	for _, finding := range findings {
		assert.Equal(t, SEV_MEDIUM, finding.Severity)
	}
}

func TestCheckHasConfiguredReporting_whenNoReporting(t *testing.T) {
	policy := "script-src 'nonce-aaaaaaaaaa'"
	findings := testCheckCSP(policy, CheckHasConfiguredReporting)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_INFO, findings[0].Severity)
	assert.Equal(t, REPORT_URI, findings[0].Directive)
}

func TestCheckHasConfiguredReporting_whenOnlyReportTo(t *testing.T) {
	policy := "script-src 'nonce-aaaaaaaaaa'; report-to name"
	findings := testCheckCSP(policy, CheckHasConfiguredReporting)
	assert.Len(t, findings, 1)
	assert.Equal(t, SEV_INFO, findings[0].Severity)
	assert.Equal(t, REPORT_TO, findings[0].Directive)
}

func TestCheckHasConfiguredReporting_whenOnlyReportUri(t *testing.T) {
	policy := "script-src 'nonce-aaaaaaaaaa'; report-uri url"
	findings := testCheckCSP(policy, CheckHasConfiguredReporting)
	assert.Empty(t, findings)
}

func TestCheckHasConfiguredReporting_whenReportUriAndReportTo(t *testing.T) {
	policy := "script-src 'nonce-aaaaaaaaaa'; report-uri url; report-to name"
	findings := testCheckCSP(policy, CheckHasConfiguredReporting)
	assert.Empty(t, findings)
}

func TestGetHostname(t *testing.T) {
	inURL := "https://www.google.com"
	assert.Equal(t, "www.google.com", getHostname(inURL))
}

func TestGetHostnamePort(t *testing.T) {
	inURL := "https://www.google.com:8080"
	assert.Equal(t, "www.google.com", getHostname(inURL))
}

func TestGetHostnameWildcardPort(t *testing.T) {
	inURL := "https://www.google.com:*"
	assert.Equal(t, "www.google.com", getHostname(inURL))
}

func TestGetHostnameNoProtocol(t *testing.T) {
	inURL := "www.google.com"
	assert.Equal(t, "www.google.com", getHostname(inURL))
}
