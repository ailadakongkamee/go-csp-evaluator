package gocspevaluator

/**
 * Set of default checks to run.
 */
var DEFAULT_CHECKS = []CheckerFn{
	CheckScriptUnsafeInline, CheckScriptUnsafeEval,
	CheckPlainUrlSchemes, CheckWildcards,
	CheckMissingDirectives,
	CheckScriptAllowlistBypass,
	CheckFlashObjectAllowlistBypass, CheckIPSource,
	CheckNonceLength, CheckSrcHttp,
	CheckDeprecatedDirective, CheckUnknownDirective,
	CheckMissingSemicolon, CheckInvalidKeyword,
}

/**
 * Strict CSP and backward compatibility checks.
 */
var STRICTCSP_CHECKS = []CheckerFn{
	CheckScriptUnsafeInline, CheckScriptUnsafeEval,
	CheckPlainUrlSchemes, CheckWildcards,
	CheckMissingDirectives,
	CheckScriptAllowlistBypass,
	CheckFlashObjectAllowlistBypass, CheckIPSource,
	CheckNonceLength, CheckSrcHttp,
	CheckDeprecatedDirective, CheckUnknownDirective,
	CheckMissingSemicolon, CheckInvalidKeyword,
}

type CSPEvaluator struct {
	version  CSPVersion
	csp      CSP
	findings []Finding
}

func NewCSPEvaluator(parsedCSP CSP, cspVersion CSPVersion) CSPEvaluator {
	return CSPEvaluator{version: cspVersion, csp: parsedCSP}
}

func (e *CSPEvaluator) Evaluate(parsedCSPChecks, effectiveCSPChecks []CheckerFn) []Finding {
	findings := make([]Finding, 0)
	checks := effectiveCSPChecks
	if len(checks) == 0 {
		checks = DEFAULT_CHECKS
	}

	// We're applying checks on the policy as it would be seen by a browser
	// supporting a specific version of CSP.
	// For example a browser supporting only CSP1 will ignore nonces and
	// therefore 'unsafe-inline' would not get ignored if a policy has nonces.
	effectiveCSP := e.csp.EffectiveCSP(e.version, findings...)

	// Checks independent of CSP version.
	for _, checkFn := range parsedCSPChecks {
		findings = append(findings, checkFn(e.csp)...)
	}

	// Checks dependent on CSP version.
	for _, checkFn := range checks {
		findings = append(findings, checkFn(effectiveCSP)...)
	}

	return findings
}
